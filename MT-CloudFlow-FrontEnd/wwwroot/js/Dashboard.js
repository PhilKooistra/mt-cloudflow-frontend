﻿var SelectedItems = "";
var Selection = [];
//var clean = [];
var YouAreHere = 0;
var GUID = [];
var depts = [];
var UserID = "";
var key_data = "";
var bulkitems = [];
var Ordering = ["custom.DueDate", "ascending"];

function UserPrefs(user) {
    document.getElementById('Results').style.display = "none";
    var UserPreferences = JSON.stringify({
        "method": "users.get_by_user_name",
        "username": user.toLowerCase(),
        "session": SessionID
    });

    console.log(UserPreferences);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(UserPreferences);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            var UserPref = JSON.parse(xhr.response);
            console.log(UserPref);
            console.log(UserPref.DefaultWorkflow);
            UserID = UserPref._id;
            if (UserPref.DefaultWorkflow == "TagMGMT") {
                window.location.href = "/TagManagement";
            }
            else if (UserPref.DefaultWorkflow == "ServiceRequest") {
                window.location.href = "/ServiceRequests";
            }
            else {
                document.getElementById('Results').style.display = "inline-block";
                document.getElementById('loadingblackout').style.display = "none";
                document.getElementById('WorkflowSelector').style.display = "inline-block";
            }
        }
    }
}


function SetWFDefault(WF) {
    var SetDefaultWF = JSON.stringify({
        "method": "users.set_keys",
        "id": UserID,
        "key_data":
        {
            "DefaultWorkflow": WF
        },
        "session": SessionID
    });
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(SetDefaultWF);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            if (WF == "TagMGMT") {
                window.location.href = "/TagManagement";
            }
            else {
                window.location.href = "/ServiceRequests";
            }
        }
    };
}



//UI handling
function ShowSearchModel() {
    document.getElementById('SearchModel').style.display = "block";
    document.getElementById('SearchModel').style.visibility = "visible";
    document.getElementById('SearchBlackout').style.display = "block";
    document.getElementById('SearchBlackout').style.visibility = "visible";
}

function CloseModel() {
    document.getElementById('SearchModel').style.display = "none";
    document.getElementById('SearchModel').style.visibility = "hidden";
    document.getElementById('SearchBlackout').style.display = "none";
    document.getElementById('SearchBlackout').style.visibility = "hidden"
    Search();
}

function ApplyFilters() {
    alert("Apply Filter Logic...")
    document.getElementById('SearchModel').style.display = "none";
    document.getElementById('SearchModel').style.visibility = "hidden";
    document.getElementById('SearchBlackout').style.display = "none";
    document.getElementById('SearchBlackout').style.visibility = "hidden"
}

function AdvancedFilteringTrigger() {
    document.getElementById('BasicFiltering').style.display = "none";
    document.getElementById('AdvancedFiltering').style.display = "block";
}

function BasicFilteringTrigger() {
    document.getElementById('AdvancedFiltering').style.display = "none";
    document.getElementById('BasicFiltering').style.display = "block";
}

function toggleleftnav() {
    if (document.getElementById('burgernav').classList.contains("hasSideNav")) {
        document.getElementById('LeftBar').style.margin = "0px 0px 0px -230px";
        document.getElementById('MainHolder').style.padding = "0px 0px 0px 10px";
        document.getElementById('DashHolder').style.width = "97vw";
        document.getElementById('burgernav').classList.remove("hasSideNav")
    }
    else {
        document.getElementById('LeftBar').style.margin = "0px 0px 0px 0px";
        document.getElementById('MainHolder').style.padding = "0px 0px 0px 235px";
        document.getElementById('DashHolder').style.width = "80vw";
        document.getElementById('burgernav').classList.add("hasSideNav")

    }
}

function PopulateDeptData() {
    //Clear Dropdown...
    $('#DeptSearch')
        .empty()
        .append('<option value=" "> </option>');

    $('BulkDeptList')
        .empty()
        .append('<option value=" "> </option>');

    var GetDepts = JSON.stringify({
        "method": "attributes.list_all",
        "session": SessionID
    })

    var xhrDepts = new XMLHttpRequest();
    xhrDepts.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhrDepts.setRequestHeader("Content-Type", "application/json");
    xhrDepts.send(GetDepts);
    xhrDepts.onreadystatechange = function () {
        if (xhrDepts.readyState === 4) {

            var x = document.getElementById("DeptSearch");
            //console.log(xhr2.response);
            var DeptData = JSON.parse(xhrDepts.response);
            var SortedList = [];
            var x = 0;
            console.log(DeptData);
            DeptData.forEach(function (x) {
                SortedList.push(x.name);
                x++;
            });
            console.log(SortedList);
            SortedList.sort();
            console.log(SortedList);

            SortedList.forEach(function (Dept) {
                // console.log(Dept);
                let HumanReadableDept = Dept;
                if (HumanReadableDept.includes('PDF')) {
                    HumanReadableDept = "PDF Proof Creation";
                }
                else {
                    HumanReadableDept = HumanReadableDept.split(/(?=[A-Z])/).join(" ");
                }
                $('#DeptSearch').append('<option value="' + HumanReadableDept + '">' + HumanReadableDept + '</option>');
                $('#BulkDeptList').append('<option value="' + HumanReadableDept + '">' + HumanReadableDept + '</option>');


                $('#SingleDeptSelect').append('<option value="' + HumanReadableDept + '">' + HumanReadableDept + '</option>');
                depts.push(Dept);

            });
            //$('#DeptSearch').val(Dept.name).trigger("chosen:updated");

        }
    }
}



//KEY Events
//Mac

//function KeyCheck(event) {
//    var KeyID = event.keyCode;
//    switch (KeyID) {
//        case 8:
//            alert("backspace");
//            break;
//        case 46:
//            alert("delete");
//            break;
//        default:
//            break;
//    }
//}

function isKeyCTRLPressed(event, _id, identifier, DueDate, ArrPos) {
    //On CTRL + Click
    if ((event.ctrlKey) || (event.metaKey)) {
        console.log(_id, identifier, DueDate)
        var selectedRow = document.getElementById(_id);
        $(selectedRow).toggleClass('Selected');
        YouAreHere = ArrPos;
        SelectedItems = $('.Selected').length
        if (SelectedItems > 0) {
            document.getElementById("BulkUpdateSelected").style.display = "block";
        }
        else {
            document.getElementById("BulkUpdateSelected").style.display = "none";
        }
    }


    //On Shift + Click
    else if (event.shiftKey) {
        // Need to work out bugs in sorting and searching...
        console.log("You are here:  " + YouAreHere);
        console.log("ArrPos:  " + ArrPos);
        console.log("You are here:  " + clean[YouAreHere]);
        console.log("GUID:  " + GUID[ArrPos]);
        var i = 0;
        var L = 0;
        if (YouAreHere > ArrPos) {
            i = ArrPos;
            L = YouAreHere;
        }
        else {
            i = YouAreHere;
            L = ArrPos;
        }
        for (i; i < L; i++) {
            console.log(GUID[i]);
            var selectedRow = document.getElementById(GUID[i]);
            $(selectedRow).toggleClass('Selected');
        }
        YouAreHere = ArrPos;
    }

    //On click toggle class 'selected'
    else {
        $('.Selected').each(function () {
            $(this).toggleClass('Selected');
        });
        console.log(_id);
        showLineItem(_id);
    }
}



function ClearSeleced() {
    //Removes selected items after a filter is applied
    Selection = [];
    document.getElementById("BulkUpdateSelected").style.display = "none";
}

function BulkUpdateSelected() {
    //Displays a list of items that will be updated on bulk screen
    bulkitems = [];
    var el = document.getElementById("BulkUpdateList")
    el.innerHTML = "<span style='font-weight:bold; display:inline-block;width:100%'>Items Being Modifed: </span><br/>";
    $('.Selected').each(function () {
        Selection.push(this.id);
        var item = $(this).closest('tr').find('td:eq(1)').text();
        var html = "" + item + "<br/>";
        $(el).append(html);
        bulkitems.push(this.id);
    });
    document.getElementById("BulkUpdateModal").style.display = "block";
    document.getElementById('BulkAssignedTo').value = " ";
    document.getElementById('BulkDeptList').value = " ";
}


function BulkSaveItems() {
    document.getElementById("loader").style.visibility = "visible";
    document.getElementById("loadingblackout").style.visibility = "visible";
    var key_data = {};
    //var duedate = document.getElementById('DueDateBulk').value;
    var BulkAssignedTo = $("#BulkAssignedTo option:selected").text();
    var SendDept = $("#BulkDeptList option:selected").text();

    //if (duedate.length != 0) {
    //    var datestring = '"custom.DueDate": ' + duedate;
    //}
    try {
        bulkitems.forEach(function (i) {

            if ((SendDept.length != 0) && (SendDept.length != " ")) {
                document.getElementById("JobDBID").value = i;
                MoveToSelectedState(SendDept, i);
            }


            if ((BulkAssignedTo.length != 0) && (BulkAssignedTo.length != " ")) {
                SetAssignee(i, BulkAssignedTo)
            }
            //alert(i)

            
        })
    }
    catch (ex) {
        alert(ex);
    }
    finally {
        document.getElementById("loader").style.visibility = "none";
        document.getElementById("loadingblackout").style.visibility = "none";
        Search();
    }
    

    //if ((SendDept.length != 0) && (SendDept != " ")) {
    //    alert("Coming soon...");
    //    //bulkitems.forEach(function (i) {
    //    //    //alert(i)
    //    //    key_data = { "custom.assignedTo": BulkAssignedTo };
    //    //    var UpdateStatusAssignedTo = JSON.stringify({
    //    //        "method": "job.set_keys",
    //    //        "id": i,
    //    //        key_data,
    //    //        "session": SessionID
    //    //    });

    //    //    console.log(UpdateStatusAssignedTo);
    //    //    var xhrStatus = new XMLHttpRequest();
    //    //    xhrStatus.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    //    //    xhrStatus.setRequestHeader("Content-Type", "application/json");
    //    //    xhrStatus.send(UpdateStatusAssignedTo);
    //    //});
    //}



    document.getElementById('BulkUpdateModal').style.display = "none";

}



function SetAssignee(i, BulkAssignedTo) {
   var dataset = { "custom.assignedTo": BulkAssignedTo };

var UpdateStatusAssignedTo = JSON.stringify({
    "method": "job.set_keys",
    "id": i,
    "key_data": dataset,
    "session": SessionID
});

console.log(UpdateStatusAssignedTo);
var xhrAssign = new XMLHttpRequest();
xhrAssign.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
xhrAssign.setRequestHeader("Content-Type", "application/json");
xhrAssign.send(UpdateStatusAssignedTo);
    xhrAssign.onreadystatechange = function () {
        if (xhrAssign.readyState === 4) {
            console.log(JSON.parse(xhrAssign.response));

        }

    }
}



//Shows a specificly selected item from a SOFI
function showLineItem(LineIdentifier) {
    $('#modal-body-right').animate({ scrollTop: 0 }, "fast");
    //alert(LineIdentifier);
    document.getElementById('LineItemModal').style.display = "block";
    document.getElementById("LineName").innerHTML = "";
    var SelectedLineItem = JSON.stringify({
        "method": "job.get_recursive",
        "id": LineIdentifier + "",
        "session": SessionID
    });
    console.log(SelectedLineItem);

    //Clear Dropdown...
    $('#AssignedToDropLineItem')
        .empty()
        .append('<option value=" "> </option>');

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(SelectedLineItem);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {

            var dataset = JSON.parse(xhr.response);


            var PMusername = dataset.custom.projectManager
            //needed for Cloudflow - Case specific usernames
            //PMusername = PMusername.toLowerCase();

            var getPM = JSON.stringify({
                "method": "users.get_by_user_name",
                "username": PMusername,
                "session": SessionID
            });

            var PMxhr = new XMLHttpRequest();
            PMxhr.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
            PMxhr.setRequestHeader("Content-Type", "application/json");
            PMxhr.send(getPM);
            PMxhr.onreadystatechange = function () {
                if (PMxhr.readyState === 4) {
                    var DueDate = dataset.custom.DueDate;
                    var HistoryURL = dataset.custom.historyURL;
                    document.getElementById("LineName").append(dataset.custom.drawingNumber);
                    //console.log(dataset.custom.DueDate);

                    if ((DueDate == undefined) || (DueDate == '')) {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth() + 1; //January is 0!
                        var yyyy = today.getFullYear();

                        if (dd < 10) {
                            dd = '0' + dd;
                        }
                        if (mm < 10) {
                            mm = '0' + mm;
                        }
                        DueDate = yyyy + '-' + mm + '-' + dd;
                        //console.log(DueDate);
                        document.getElementById("DueDate").value = DueDate;
                    }
                    else {
                        var arr = DueDate.split("-");
                        //console.log(arr[0] + "-" + arr[1] + "-" + arr[2].substring(0, 2));
                        DueDate = (arr[0] + "-" + arr[1] + "-" + arr[2].substring(0, 2));
                        document.getElementById("DueDate").value = DueDate;
                    };

                    //Gets a list of users defined in CloudFlow DB
                    var GetUsers = JSON.stringify({
                        "method": "users.list_with_options",
                        "query": ["fullname", "not equal to", "Administrator", "and", "fullname", "not equal to", "Guest", "and", "fullname", "not equal to", "Eventhandler"],
                        "order_by": ["fullname", "ascending"],
                        "fields": [],
                        "options": { "first": 0, "maximum": 100 },
                        "session": SessionID
                    });
                    var xhr2 = new XMLHttpRequest();
                    xhr2.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
                    xhr2.setRequestHeader("Content-Type", "application/json");
                    xhr2.send(GetUsers);
                    xhr2.onreadystatechange = function () {
                        if (xhr2.readyState === 4) {
                            var x = document.getElementById("AssignedToDropLineItem");
                            var UserDataset = JSON.parse(xhr2.response);
                            var JobDBID = document.getElementById('JobDBID');
                            var Project_ID = document.getElementById('Project_ID');

                            Project_ID.value = dataset.project_id;

                            var PMDATA = JSON.parse(PMxhr.response);
                            var PM = PMDATA.fullname;

                            console.log(PMDATA);
                            console.log("Project Manager:  " + PM);
                            JobDBID.value = dataset._id + "|" + dataset.custom.flow.onlineProof + "|" + dataset.custom.flow.breederProof + "|" + dataset.custom.flow.customerProof + "|" + PM + "|" + dataset.custom.flow.epsonProof + "|" + dataset.project_id + "|" + dataset.custom.flow.pdfProof + "|" + dataset.custom.drawingNumber;

                            UserDataset.results.forEach(function (UserObj) {
                                //console.log(UserObj.fullname);
                                $('#AssignedToDropLineItem').append('<option value="' + UserObj.fullname + '">' + UserObj.fullname + '</option>');

                            });
                            $('#AssignedToDropLineItem').val(dataset.custom.assignedTo).trigger("chosen:updated");

                        }
                    }


                    $('#AssignedToDropLineItem').val(dataset.custom.assignedTo).trigger("chosen:updated");

                    $('#SingleDeptSelect').val(dataset.state).trigger("chosen:updated");

                    document.getElementById("JobStatus").innerHTML = "";

                    document.getElementById("JobStatus").innerHTML = dataset.status;

                    document.getElementById("JobDescriptions").innerHTML = dataset.description;

                    document.getElementById("TCROrderInstructions").innerHTML = dataset.projects[0].custom.TCROrderInstructions;
                    document.getElementById("TCRItemInstructions").innerHTML = dataset.projects[0].custom.TCRItemInstructions;
                    document.getElementById("PreProdInstructions").innerHTML = dataset.projects[0].custom.PreProdComments;
                    document.getElementById("Notes").value = dataset.custom.Notes;

                    document.getElementById("DrawNumLink").innerHTML = "";
                    document.getElementById("DrawNumLink").innerHTML = "<a href='" + dataset.projects[0].custom.tagManagementLink + "'target='_blank'>" + dataset.projects[0].custom.drawingNumber + "</a>";
                    //Null value handling 
                    //- most likely will go away once valid data supplied
                    if ((HistoryURL == undefined) || (HistoryURL == '')) {
                        document.getElementById("JobHistory").style.display = "none";
                        document.getElementById("JobHistoryNone").style.display = "inline-block";

                    }
                    else {

                        document.getElementById("JobHistory").style.display = "inline-block";
                        document.getElementById("JobHistoryNone").style.display = "none";
                        document.getElementById("JobHistory").setAttribute("onClick", "OpenHistory('" + dataset.project_id + "')");
                        //document.getElementById("JobHistory").setAttribute("onClick", "javascript: window.open('" + HistoryURL + "', '_blank');");
                    };

                    var custemail = dataset.custom.customer.email;
                    if ((custemail != undefined) && (custemail != '')) {
                        document.getElementById("CustEmail").value = custemail;
                    }

                    var custcontact = dataset.custom.customer.contact;
                    if ((custcontact != undefined) && (custcontact != '')) {
                        document.getElementById("ContactPerson").value = custcontact;
                    }

                    var account = dataset.custom.customer.account;
                    if ((account != undefined) && (account != '')) {
                        document.getElementById("CustAcct").value = account;
                    }

                    var onlineproof = dataset.custom.flow.onlineProof;
                    if ((onlineproof != undefined) && (onlineproof != '')) {
                        if (dataset.custom.flow.onlineProof == "no") {
                            var radiobtn = document.getElementById("OnlineProofNo");
                            radiobtn.checked = true;
                        }
                        else {
                            var radiobtn = document.getElementById("OnlineProofYes");
                            radiobtn.checked = true;
                        };
                    }


                    var breederproof = dataset.custom.flow.breederProof;
                    if ((breederproof != undefined) && (breederproof != '')) {
                        if (dataset.custom.flow.breederProof == "N") {
                            var radiobtn = document.getElementById("BreedProofNo");
                            radiobtn.checked = true;
                        }
                        else {
                            var radiobtn = document.getElementById("BreedProofYes");
                            radiobtn.checked = true;
                        };
                        // document.getElementById("BreedProofEmail").value = dataset.custom.flow.breederEmail;
                    }

                    breederName = dataset.custom.flow.breederName;
                    if ((breederproof == undefined) || (breederproof == '')) {
                        document.getElementById("BreederName").value = " ";
                    }
                    else {
                        document.getElementById("BreederName").value = breederName;
                    }


                    //var custproof = dataset.custom.flow.customerProof;
                    //console.log("Cust Proof: " + custproof);
                    //if ((custproof != undefined) && (custproof != '')) {
                    //    if (dataset.custom.flow.customerProof == "N") {
                    //        var radiobtn = document.getElementById("CustProofNo");
                    //        radiobtn.checked = true;
                    //    }
                    //    else {
                    //        var radiobtn = document.getElementById("CustProofYes");
                    //        radiobtn.checked = true;
                    //    };
                    //    document.getElementById("CustProofEmail").value = dataset.custom.flow.customerEmail;
                    //};

                    var epsonProof = dataset.custom.flow.epsonProof;
                    if ((epsonProof != undefined) && (epsonProof != '')) {

                        if (epsonProof == "N") {
                            var radiobtn = document.getElementById("EpsonProofNo");
                            radiobtn.checked = true;
                        }
                        else {
                            var radiobtn = document.getElementById("EpsonProofYes");
                            radiobtn.checked = true;
                        };
                    };

                    var PDFProof = dataset.custom.flow.pdfProof;
                    if ((PDFProof != undefined) && (PDFProof != '')) {

                        if (PDFProof == "N") {
                            var radiobtn = document.getElementById("pdfProofNo");
                            radiobtn.checked = true;
                        }
                        else {
                            var radiobtn = document.getElementById("pdfProofYes");
                            radiobtn.checked = true;
                        };
                    };

                    console.log(dataset.next_states);
                    if (typeof dataset.next_states === 'undefined') {
                        document.getElementById('ModalButtonsTop').innerHTML = "";
                        //do nothing
                        // TODO:  Need a button that sets status to "complete"
                    }
                    else {
                        var i = 0;
                        var el = document.getElementById('ModalButtonsTop');
                        document.getElementById('ModalButtonsTop').innerHTML = "";
                        dataset.next_states.forEach(function () {
                            //Creates button for each next available 
                            //option in workflow
                            var html = "<button onclick='MoveToSelectedState(\"" + dataset.next_states[i] + "\",\"" + dataset.project_id + "\")'>" + dataset.next_states[i] + "</button>"
                            $(el).append(html);
                            i++;
                        });
                    };
                };
            };

        };
    };
};

function LoadBulkUsers() {
    //Gets a list of users defined in CloudFlow DB
    var GetUsers = JSON.stringify({
        "method": "users.list_with_options",
        "query": ["fullname", "not equal to", "Administrator", "and", "fullname", "not equal to", "Guest", "and", "fullname", "not equal to", "Eventhandler"],
        "order_by": ["fullname", "ascending"],
        "fields": [],
        "options": { "first": 0, "maximum": 100 },
        "session": SessionID
    });
    var xhr2 = new XMLHttpRequest();
    xhr2.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhr2.setRequestHeader("Content-Type", "application/json");
    xhr2.send(GetUsers);
    xhr2.onreadystatechange = function () {
        if (xhr2.readyState === 4) {
            var UserDataset = JSON.parse(xhr2.response);
            UserDataset.results.forEach(function (UserObj) {
                //console.log(UserObj.fullname);
                $('#BulkAssignedTo').append('<option value="' + UserObj.fullname + '">' + UserObj.fullname + '</option>');

            });

        }
    }
}


function OpenHistory(projectID) {
    document.getElementById('fullTable').innerHTML = "";
    document.getElementById("open-modal").style.display = "block";
    document.getElementById("loadingblackout").style.visibility = "visible";
    var table = $('<table id="rowTable">' +
        '<caption><b>ITEM HISTORY</b></caption>' +
        '<thead>' +
        '<th>Date</th>' +
        '<th>Message</th>' +
        '</thead>');
    var session = SessionID;
    var datafromfield = document.getElementById("JobDBID").value;
    var splitField = datafromfield.split("|");
    var JobID = splitField[0];

    var GetJobDetails = JSON.stringify({
        "method": "job.get",
        "id": JobID,
        "session": SessionID
    });
    var xhrJob = new XMLHttpRequest();
    xhrJob.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhrJob.setRequestHeader("Content-Type", "application/json");
    xhrJob.send(GetJobDetails);
    xhrJob.onreadystatechange = function () {
        if (xhrJob.readyState === 4) {
            var results = JSON.parse(xhrJob.response);
            var history = results.custom.history;
            console.log(results);
            if (history.length) {

                //var x = 0;
                history.push({ date: "01/01/1980", message: "Filler Object - Bug in array" });
                for (x = history.length - 1; x > 0; x--) {
                    //history.forEach(function () {
                    var row = $('<tr class="spaceUnder proof">')

                    var theMessage = history[x].message;
                    var d = new Date(history[x].date);
                    var month = d.getUTCMonth() + 1;
                    var day = d.getUTCDate();
                    var year = d.getUTCFullYear();
                    var hour = d.getUTCHours();
                    var min = d.getUTCMinutes()
                    var sec = d.getUTCSeconds();

                    row.append(
                        '<td>' + month + '/' + day + '/' + year + ' ' + hour + ':' + min + ':' + sec + '</td>' +
                        '<td>' + theMessage + '</td>');
                    $('#rowTable').append(row);
                    $('#fullTable').append(table);
                    //x++;
                };
            }
        }
    }
}


function DeptChange() {
    var Project_ID = document.getElementById('Project_ID').value;
    var DeptSelected = document.getElementById('SingleDeptSelect').value;

    console.log(DeptSelected, Project_ID);
    MoveToSelectedState(DeptSelected, Project_ID);
}


function datachange(type, field, value) {

    var Fieldkey = field;
    var Value = value

    var datafromfield = document.getElementById("JobDBID").value;
    var splitField = datafromfield.split("|");
    var JobID = splitField[0];
    var OP = splitField[1];
    var BP = splitField[2];
    var CP = splitField[3];
    var EP = splitField[5];
    var PM = splitField[4];
    var ProjectID = splitField[6];
    var PDFP = splitField[7];
    var data = '"' + Fieldkey + '":' + '"' + Value + '"';

    var GetJobDetails = JSON.stringify({
        "method": "job.get",
        "id": JobID,
        "session": SessionID
    });
    var xhrJob = new XMLHttpRequest();
    xhrJob.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhrJob.setRequestHeader("Content-Type", "application/json");
    xhrJob.send(GetJobDetails);
    xhrJob.onreadystatechange = function () {
        if (xhrJob.readyState === 4) {
            var results = JSON.parse(xhrJob.response);
            console.log(results);
            var history = results.custom.history;
            history.push({
                "date": new Date(),
                "message": Fieldkey + " was updated to " + Value + " by " + userFullName
            });

            change_type = ""
            change_type = type + ".set_keys";
            var id = "";
            if (type = "job") {
                id = JobID;
            }
            else if (type = "project") {
                id = ProjectID;
            }

            key_data = {};
            key_data[Fieldkey] = Value;
            key_data["custom.history"] = history;

            console.log(JSON.stringify(key_data));
            var update = JSON.stringify({
                "method": change_type,
                "id": id,
                key_data,
                "session": SessionID
            });

            console.log(update);
            var xhrStatus = new XMLHttpRequest();
            xhrStatus.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
            xhrStatus.setRequestHeader("Content-Type", "application/json");
            xhrStatus.send(update);
            if (xhrStatus.readyState === 4) {
            }
        }
    }

}




function MoveToSelectedState(SelectedState, project_id) {
    console.log(SelectedState, project_id);
    var state = SelectedState;
    var attrName = '';
    var methodname = '';
    var datafromfield = document.getElementById("JobDBID").value;
    var splitField = datafromfield.split("|");
    var JobID = splitField[0];
    var OP = splitField[1];
    var BP = splitField[2];
    var CP = splitField[3];
    var EP = splitField[5];
    var PM = splitField[4];
    var ProjectID = splitField[6];
    var PDFP = splitField[7];
    var DrawingNumber = splitField[8];

    var id = project_id;

    var UpdateStatusAssignedTo;

    if ((state == "No Updates Needed") && (EP == "N") && (PDFP == "N")) {
        //Different call because this triggers the cloudflow proofing
        UpdateStatusAssignedTo = JSON.stringify({
            "method": "job.set_state",
            "project_id": project_id,
            "state": state,
            "session": SessionID
        });
    }
    if (state == "Final Processing") {
        FinalizeItem(JobID,DrawingNumber);
        return
    }
    else {
        //Set state based on button click
        setStatus(state, OP, BP, CP, EP, PM, PDFP, id);
        UpdateStatusAssignedTo = JSON.stringify({
            "method": "job.set_keys",
            "id": JobID,
            key_data,
            "session": SessionID
        });
    }
    console.log(UpdateStatusAssignedTo);
    var xhrStatus = new XMLHttpRequest();
    xhrStatus.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhrStatus.setRequestHeader("Content-Type", "application/json");
    xhrStatus.send(UpdateStatusAssignedTo);
    if (xhrStatus.readyState === 4) {
        console.log(xhrStatus.response);
    }

    function sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }

    async function refresh() {
        //Cosmetic to show the task is processing

        document.getElementById("loader").style.visibility = "visible";
        document.getElementById("loadingblackout").style.visibility = "visible";
        await sleep(1500);
        //showLineItem(JobID);
        Search();
        document.getElementById("loader").style.visibility = "hidden";
        document.getElementById("loadingblackout").style.visibility = "hidden";
        document.getElementById('LineItemModal').style.display = "none";
    }

    refresh();

}


function GatherHistory(state, id) {
    //var datafromfield = document.getElementById("JobDBID").value;
    //var splitField = datafromfield.split("|");
    //var JobID = splitField[0];
    JobID = id

    var GetJobDetails = JSON.stringify({
        "method": "job.get",
        "id": JobID,
        "session": SessionID
    });
    var xhrJob = new XMLHttpRequest();
    xhrJob.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhrJob.setRequestHeader("Content-Type", "application/json");
    xhrJob.send(GetJobDetails);
    xhrJob.onreadystatechange = function () {
        if (xhrJob.readyState === 4) {
            var results = JSON.parse(xhrJob.response);
            var history = results.custom.history;
            history.push({
                "date": new Date(),
                "message": results.name + " was updated to new state: '" + state + "' by " + userFullName
            });
            key_data = { "custom.history": history };
            console.log(history);
            var UpdateHistory = JSON.stringify({
                "method": "job.set_keys",
                "id": JobID,
                key_data,
                "session": SessionID
            });

            var xhrHistory = new XMLHttpRequest();
            xhrHistory.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
            xhrHistory.setRequestHeader("Content-Type", "application/json");

            console.log(UpdateHistory);
            xhrHistory.send(UpdateHistory);
            if (xhrHistory.readyState === 4) {
                console.log(xhrHistory.response);
            }
        }
    }
}

//NEED TO ADD ALL NEXT STATES TO CASES

function setStatus(state, OP, BP, CP, EP, PM, PDFP, id) {
    var Newstate = "";
    switch (state) {
        //Cust Service and Scheduling
        case "Scheduling":
            key_data = { "status": "Waiting to be Scheduled", "state": "Scheduling", "custom.assignedTo": " ", "next_states": ["Hort"], "custom.history": history };
            attrName = 'Scheduling';
            Newstate = "Scheduling";
            break;
        case "Customer Service":
            key_data = { "status": "Question on Order", "state": "Customer Service", "custom.assignedTo": PM, "next_states": ["Hort"] }; //Sets the PM to assigned
            attrName = 'CustomerService';
            Newstate = "Customer Service";
            break;


        //Photo Selection
        case "Hort":
            key_data = { "status": "Product text being created", "state": "Hort", "custom.assignedTo": " ", "next_states": ["Image Research"] };
            attrName = 'Hort';
            Newstate = "Hort";
            break;
        case "Image Research":
            key_data = { "status": "Images being selected", "state": "Image Research", "custom.assignedTo": " ", "next_states": ["Image Editing", "Image Outsourcing", "Continue to Review"] };
            attrName = 'ImageEditing';
            Newstate = "Image Research";
            break;
        
        case "Image Outsourcing":
            key_data = { "status": "Image being outsourced", "state": "Image Outsourcing", "custom.assignedTo": " ", "next_states": ["Review"] };
            attrName = 'ImageOutsourcing';
            Newstate = "Image Outsourcing";
            break;


        case "Image Editing":
            key_data = { "status": "Images being selected", "state": "Image Editing", "custom.assignedTo": " ", "next_states": ["Review"] };
            attrName = 'ImageEditing';
            Newstate = "Image Research";
            break;
        //Reviews
        case "Continue to Review":
            key_data = { "status": "Product being reviewed", "state": "Review", "custom.assignedTo": " ", "next_states": ["No Updates Needed"] };
            attrName = 'Review';
            Newstate = "Continue to Review";
            break;
        case "Review":
            key_data = { "status": "Product being reviewed", "state": "Review", "custom.assignedTo": " ", "next_states": ["No Updates Needed"] };
            attrName = 'Review';
            Newstate = "Review";
            break;

        //case "Updates Required":
        //    //Sends file back to the start of Photos
        //    key_data = { "status": "Product text being created", "state": "Hort", "custom.assignedTo": " ", "next_states": ["Image Research"] };
        //    attrName = 'Hort';
        //    Newstate = "Updates Required";
        //    break;


        case "Breeder Proofs Creation":
            key_data = { "status": "Product being reviewed", "state": "Review", "custom.assignedTo": PM, "next_states": ["Breeder Approval"] };
            attrName = 'Review';
            Newstate = "Review";
            break;

        //HERE is the main logic section
        case "Review Complete":
            if (BP == "Y") {
                //How do we handle logic?
                //If Breederproof, Send to Proofscope?
                key_data = { "status": "Breeder Proof Creation", "state": "Breeder Proof Creation", "custom.assignedTo": " ", "next_states": ["Breeder Approval"] };
                attrName = 'Proofs';
                Newstate = "Breeder Proofing";
            }
            else if (BP == "N" && CP == "Y") {
                //No breeder, Yes Cust Proof - To Proofscope...
                key_data = { "status": "Product being reviewed", "state": "Customer Approval", "custom.assignedTo": " ", "next_states": ["Customer Approval"] };
                attrName = 'Proofs';
                Newstate = "Customer Proofing";
            }
            else if (BP == "N" && OP == "Y") {
                //Go To Online Proofing
                key_data = { "status": "Product being placed online", "state": "Online Approval", "custom.assignedTo": " ", "next_states": ["Online Approval"] };
                attrName = 'Proofs';
                Newstate = "Online Proofing";
            }
            else if (BP == "N" && EP == "Y") {
                //Go to Epson Proofing
                key_data = { "status": "Product being reviewed", "state": "Epson Creation", "custom.assignedTo": " ", "next_states": ["Epson Proof Creation"] };
                attrName = 'Proofs';
                Newstate = "Epson Proofing";
            }
            else if (BP == "N" && PDFP == "Y") {
                //Go to PDF Proofing
                key_data = { "status": "Product being reviewed", "state": "Proofs Staging", "custom.assignedTo": PM, "next_states": ["PDF Proof Creation"] };
                attrName = 'Proofs';
                Newstate = "PDF Proofing";
            }

            else {
                //No Proofs
                key_data = { "status": "Product being reviewed", "state": "No Updates Needed", "custom.assignedTo": " ", "next_states": ["Final Processing"] };
                attrName = 'Proofs';
                Newstate = "No Updates Needed";
            }
            break;


        //If Breeder Approval is Yes.
        case "Breeder Approval":
            if (CP == "Y") {
                //No breeder, Yes Cust Proof - To Proofscope...
                key_data = { "status": "Product being reviewed", "state": "Ready for Proofing", "custom.assignedTo": " ", "next_states": ["Customer Approval"] };
                attrName = 'Proofs';
                Newstate = "Customer Proofing";
            }
            else if (OP == "Y") {
                //Go To Online Proofing
                key_data = { "status": "Product being placed online", "state": "Ready for Proofing", "custom.assignedTo": " ", "next_states": ["Online Approval"] };
                attrName = 'Proofs';
                Newstate = "Online Proofing";
            }
            else if (EP == "Y") {
                //Go to Epson Proofing
                key_data = { "status": "Proof being created", "state": "Ready for Proofing", "custom.assignedTo": " ", "next_states": ["Epson Proof Creation"] };
                attrName = 'Proofs';
                Newstate = "Epson Proofing";
            }
            else if (PDFP == "Y") {
                key_data = { "status": "Product being reviewed", "state": "Proof Staging", "custom.assignedTo": PM, "next_states": ["PDF Proof Creation"] };
                attrName = 'Proofs';
                Newstate = "PDF Proofing";
            }
            else {
                key_data = { "status": "Product being reviewed", "state": "Breeder Approval", "custom.assignedTo": " ", "next_states": ["Final Processing"] };
                attrName = 'BreederApproval';
                Newstate = "Breeder Approved";
            }
            break;

        case "Customer Approval":
            key_data = { "status": "Product waiting on customer approval", "state": "Customer Approval", "custom.assignedTo": PM, "next_states": ["Final Processing"] };
            attrName = 'CustomerApproval';
            Newstate = "Customer Approval";
            break;

        case "Online Approval":
            key_data = { "status": "Product Going Online", "state": "Online Approval", "custom.assignedTo": " ", "next_states": ["Associate Season"] };
            attrName = 'OnlineProof';
            Newstate = "Online Approval";
            break;
        case "Associate Season":
            key_data = { "status": "Question on Order", "state": "Associate Season", "custom.assignedTo": " ", "next_states": ["End"] };
            attrName = 'OnlineProof';
            Newstate = "Associate to Season";
            break;

        case "Epson Proof Creation":
            key_data = { "status": "Proof Being Created", "state": "Epson Proof Creation", "custom.assignedTo": " ", "next_states": ["Customer Approval"] }; //Think this will fail bc it will trigger proofscope
            attrName = 'EpsonProof';
            Newstate = "Epson Proof Creation";
            break;

        case "PDF Proof Creation":
            key_data = { "status": "Proof Being Created", "state": "PDF Proof Creation", "custom.assignedTo": " ", "next_states": ["Customer Approval"] }; //Think this will fail bc it will trigger proofscope
            attrName = 'EpsonProof';
            Newstate = "Epson Proof Creation";
            break;


        case "Merge":
            key_data = { "status": "Product being built", "state": "Merge", "custom.assignedTo": " ", "next_states": ["Review"] };
            attrName = 'Merge';
            Newstate = "Merge";
            break;



        

            //key_data = { "status": "Making item Active", "state": "Final Processing", "custom.assignedTo": " ", "next_states": ["End"] };
            ////This should change to PM
            //attrName = 'CustomerService';
            //Newstate = "Final Processing";
            //break;

            case "End":
            key_data = { "status": "Complete", "state": "End", "custom.assignedTo": " " };
            attrName = 'CustomerService';
            Newstate = "Complete";
            break;
    }
console.log("KeyData: " + key_data);
GatherHistory(Newstate, id);
}


function JobHistory() {
    //Pulls up job history based on Job ID
    var JobID = document.getElementById('JobID').value;
    var SelectedLineItem = JSON.stringify({
        "method": "job.get",
        "id": document.getElementById('JobID').value,
        "session": SessionID
    });
    //console.log(SelectedLineItem);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(SelectedLineItem);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(xhr.response);
            var dataset = JSON.parse(xhr.response);
            //Opens history in new window
            window.open(dataset.historyURL, '_blank');
        }
    }
}



function UpdateAssignedTo() {
    JobID = document.getElementById("JobDBID").value
    var Assignee = AssignedToDropLineItem.value
    var AssignUser = JSON.stringify({
        "method": "job.set_keys",
        "id": JobID,
        "key_data":
        {
            "custom.assignedTo": Assignee
        }
        ,
        "session": SessionID
    });
    //console.log(AssignUser);

    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(AssignUser);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
            console.log(Assignee);
            showLineItem(JobID);
        }
    }

}


function UserOptions(e) {
    if (e.ctrlKey && e.shiftKey) {
        showaboutmodal();
    };
}

function showaboutmodal() {
    document.getElementById("AboutModal").style.display = "block";
    document.getElementById("loadingblackout").style.visibility = "visible";
}


function Search() {
    document.getElementById("loader").style.visibility = "hidden";
    document.getElementById("loadingblackout").style.visibility = "hidden";
    var requestdata = '';
    var DeptSelected = document.getElementById("DeptSearch");
    DeptSelected = DeptSelected.options[DeptSelected.selectedIndex].value;
    var SearchText = document.getElementById("SOFISearch").value;

    var DueToday = document.getElementById('DueToday');


    var querydata = ["template", "does not exist", "and", "parent_project_id", "does not exist", "and", "event_handler.whitepaper", "contains text like", "1_MasterTag_JOBS_DB", "and", "state", "not equal to", "End"]

    //Set due date if checked
    if (DueToday.checked == true) {

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        today = yyyy + '-' + mm + '-' + dd;


        querydata.push("and");
        querydata.push("custom.DueDate");
        querydata.push("contains");
        querydata.push(today);
    }

    //If Dept field is empty/blank
    if (DeptSelected.length > 1) {
        querydata.push("and");
        querydata.push("state");
        querydata.push("equal to");
        querydata.push(DeptSelected);
    }

    //If AssignedToMe is checked
    if (AssignedToMe.checked == true) {
        querydata.push("and");
        querydata.push("custom.assignedTo");
        querydata.push("equal to");
        querydata.push(userFullName);
    }

    //If CSRMyStuff is checked
    if (CSRMyStuff.checked == true) {
        querydata.push("and");
        querydata.push("custom.projectManager");
        querydata.push("equal to");
        querydata.push(user);
    }

    //If search string isn't empty
    if (SearchText.length > 0) {
        querydata.push("and");
        querydata.push("identifier");
        querydata.push("contains");
        querydata.push(SearchText);
    }

    requestdata = JSON.stringify({
        "method": "job.list_with_options",
        "query": querydata,
        "order_by": Ordering,
        "fields": [],
        "options": {
            "first": 0,
            "maximum": 100
        },
        "session": SessionID
    });

    console.log(requestdata);
    var promises = [];
    var xhr = new XMLHttpRequest();
    xhr.open("POST", "http://nodetst.mastertag.com/api/cloudflow/");
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.send(requestdata);
    xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {

            console.log(xhr.response);
            var el = document.getElementById("MainData");

            $("#MainData").find("tr:not(:first)").remove();
            var Projects = JSON.parse(xhr.response);

            var clean = [];
            if (Projects.results.length > 0) {

                Projects.results.forEach(function (obj) {
                    var DueDate = obj.custom.DueDate;
                    if ((DueDate == undefined) || (DueDate == '')) {
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth() + 1; //January is 0!
                        var yyyy = today.getFullYear();

                        if (dd < 10) {
                            dd = '0' + dd;
                        }
                        if (mm < 10) {
                            mm = '0' + mm;
                        }
                        DueDate = mm + '-' + dd + "-" + yyyy;

                    }
                    else {
                        var arr = DueDate.split("-");
                        // console.log(arr[0] + "-" + arr[1] + "-" + arr[2].substring(0, 2));
                        DueDate = (arr[1] + "-" + arr[2].substring(0, 2) + "-" + arr[0]);
                    };
                    var ItemStatus = "";

                    //
                    var dateDue = new Date(DueDate);
                    var today = new Date();
                    today.setHours(0, 0, 0, 0);
                    dateDue.setHours(0, 0, 0, 0);
                    if (dateDue < today) {
                        ItemStatus = "LateItem";
                    }
                    else if (dateDue > today) {

                    }

                    //console.log(dateDue);
                    //console.log(Date());

                    //Build table based on dataset
                    var html =
                        "<tr id='" + obj._id + "' class='dataset " + ItemStatus + "' onclick='isKeyCTRLPressed(event, \"" + obj._id + "\",\"" + obj.identifier + "\",\"" + DueDate + "\"," + i + ")'>" +
                        "<td class='cell'>" + obj.identifier + "</td > " +
                        "<td class='cell'>" + obj.custom.drawingNumber + "</td > " +
                        "<td class='cell'>" + obj.custom.shape + "</td>" +
                        "<td class='cell'>" + DueDate + "</td>" +

                        "<td class='cell'>" + obj.custom.customer.name + "</td >" +
                        "<td class='cell'>" + obj.custom.assignedTo + "</td>" +
                        "<td class='cell'>" + obj.state + "</td>" +
                        "</tr>"
                    $(el).append(html);
                    GUID.push(obj._id);

                })
            }
            else {
                var html =
                    "<tr class='dataset'>" +
                    "<td class='cell'> No Results found for this search...</td > " +
                    "</tr>"
                $(el).append(html);
            }
            ClearSeleced();
        }
    }
}









function SetStatusActiveTM(DrawingNumber) {


}



function sortTable(n) {
    if (Ordering.includes("descending")) {
        Ordering = ["custom.DueDate", "ascending"];
        console.log("row" + n + "img");
    }
    else {
        Ordering = ["custom.DueDate", "descending"];
        document.getElementById("row" + n + "img").src = "/images/icon-sort-asc.svg"
    }
    Search();
}


//Export CSV processes
function download_csv(csv, filename) {
    var csvFile;
    var downloadLink;
    // CSV FILE
    csvFile = new Blob([csv], { type: "text/csv" });
    // Download link
    downloadLink = document.createElement("a");
    // File name
    downloadLink.download = filename;
    // We have to create a link to the file
    downloadLink.href = window.URL.createObjectURL(csvFile);
    // Make sure that the link is not displayed
    downloadLink.style.display = "none";
    // Add the link to your DOM
    document.body.appendChild(downloadLink);
    // Download
    downloadLink.click();
}

function export_table_to_csv(filename) {
    var csv = [];
    var rows = document.querySelectorAll("#MainData tr");

    for (var i = 0; i < rows.length; i++) {
        var row = [], cols = rows[i].querySelectorAll("td, th");
        for (var j = 0; j < cols.length; j++)
            row.push(cols[j].innerText);
        csv.push(row.join(","));
    }
    // Download CSV
    download_csv(csv.join("\n"), filename);
}

function ExportTable() {
    //var html = document.querySelector("MainData").outerHTML;
    export_table_to_csv("Workflow Export - " + Date() + ".csv");
};


function SearchSlideOut() {
    document.getElementById('SlideoutSearch').style.width = '730px';
    document.getElementById('SlideoutSearch').style.display = 'inline-block';

}