﻿using Microsoft.AspNetCore.Http;
using System;
using System.DirectoryServices.AccountManagement;
using System.Net;
using System.Text;
using System.DirectoryServices;
using RestSharp; //calling rest APIs
using Newtonsoft.Json; //using Json.Net
using Newtonsoft.Json.Linq; //using Json.Net

using System.Runtime.Serialization.Json;



public static class Helper
    {
    
    public static string UserFullName(string user)
    {
        try
        {
            using (var context = new PrincipalContext(ContextType.Domain))
            {
                var principal = UserPrincipal.FindByIdentity(context, user);
                System.Diagnostics.Debug.WriteLine(principal.DisplayName);
                if (principal.DisplayName != null)
                {
                    var fullName = string.Format("{0} {1}", principal.GivenName, principal.Surname);
                    return fullName;
                }
                else
                {
                    return "nothing";
                }
            }
        }
        catch{
            return "User Not Defined";
                }
    }



    public static string Auth()
    {


        string test = "{\"method\": \"auth.create_session\",\"user_name\": \"admin\",\"user_pass\": \"admin\"}";
        try
        {
            using (var client = new WebClient { UseDefaultCredentials = true })
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "application/xml; charset=utf-8");
                byte[] responseArray = client.UploadData("http://nodetst.mastertag.com/api/cloudflow/", "POST", Encoding.UTF8.GetBytes(test));
                string response = Encoding.ASCII.GetString(responseArray);
                return response;
            }
        }
        catch
        {
            return "Failure";
        }
    }

    

    public static string TMGetAuthorizationAPI()
    {
        var userName = "templates@mastertag.com";
        var password = "T@gsrus1!!#MT2019";
        var tokenURL = "https://test-online.mastertag.com/token";

        string TMUOFAccess = "grant_type=password&username=" + userName + "&password=" + password + "&undefined=";

        //var client = new RestClient("https://test-online.mastertag.com/token");
        var client = new RestClient(tokenURL);
        var request = new RestRequest(Method.POST);
        request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        request.AddParameter("undefined", TMUOFAccess, ParameterType.RequestBody);
        IRestResponse authResponse = client.Execute(request);

        //retrive the token using Json.net
        string JSONAuthString = authResponse.Content.ToString();
        //string ExpiresValue;
        //string TokenTypeValue;
        string TokenValue;
        string APIError;

        JObject authobj = JObject.Parse(JSONAuthString);
        //ExpiresValue = (string)authobj.SelectToken("expires_in");
        //TokenTypeValue = (string)authobj.SelectToken("token_type");
        TokenValue = "Bearer " + (string)authobj.SelectToken("access_token");

        //will be null if there is no error
        APIError = (string)authobj.SelectToken("error");
        
        return TokenValue;
    }
}